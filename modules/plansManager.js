const dataManager = require('./dataManager');

class plansManager {

    constructor (oDataCollector){
        this.oDataManager = new dataManager;
        this.loadedPlan = false;

        this.loadFullPlan();
    }
    


    /// PLANS SETUP AND SAVE
    searchPlans (){

    }

    loadFullPlan (){
        this.oDataManager.getGrowingPlan().then(data => {
            this.loadedPlan = data;
            this.oDataManager.growPlanIdentifier = data[0]._id;
            //console.log('plan is now loaded and saved in object, and will be presented after this message');
            //console.log(this.loadedPlan)
        })
    }

    addAlgorithems (){

    }

    saveFinalizedPlanToDB (){
        // pick only relevant sensors & switches - plan will contain all available options! 
    }

    /// PLANS LOAD INTO BOT
    loadTodayPlan (){
       
    }

    
    timeConverter(UNIX_timestamp){
        var a = new Date(UNIX_timestamp * 1000);
        var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
        var year = a.getFullYear();
        var month = months[a.getMonth()];
        var date = a.getDate();
        var hour = a.getHours();
        var min = a.getMinutes();
        var sec = a.getSeconds();
        var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
        return time;
    }

    startPlan (){
        let daysInPlan = 0;
        let ONE_DAY = 60 * 60 * 24;
        let todayDate = new Date().getTime()/1000;
        if(!this.loadedPlan){
            console.log('plan was not yet loaded')
            return false;
        }
        
        daysInPlan = Object.keys(this.loadedPlan[0].dailyPlan).length;

        let finishDate = (todayDate + daysInPlan*ONE_DAY);

        
        //save finish day
        console.log(this.timeConverter(finishDate));
    }
    
    loadPlanByDayId (iId){
       const dailyPlanRecords = this.loadedPlan;
       if(!dailyPlanRecords)
            return false;
        else 
            return dailyPlanRecords[0].dailyPlan[iId];
            
    }

    getCurrentDayFromPlan(){

    }

    addAlgorithemsPerDayByCurrentStatus (){

    }

    

}

module.exports = plansManager;