/*const sensorManager = require('./modules/sensorManager');
oSensorManager = new sensorManager();
*/

class deviceManager {

	constructor(url, request, ip){
		this.sensorDataArray = [];
		this.sensorArray = [];
		this.switchesArray = [];
		this.url = url;
		this.request = request;
		this.ip = ip;
		this.interval = [];
	//	this.sensorManager = sensorManager;
		this.statuses = [];
		/*if(url == null)
			this.traceDeviceOnNetwork(ip)*/
		return this;
		
	}

	saveSensorData (sensor, sensorData){
		if(this.sensorDataArray[sensor] == undefined)
			this.sensorDataArray[sensor] = [];

		/*if(this.sensorDataArray[sensor].length >0){
			this.sensorDataArray[sensor][this.sensorDataArray[sensor].length] = sensorData;
		} else {
		*/	this.sensorDataArray[sensor].push(sensorData);	

		let tmp = this.getSensorData(sensor);
		//console.log(tmp)
		//}

		
	}

	getSensorData (sensor){
		return this.sensorDataArray[sensor];
	}
	getAllSensorData (){
		return this.sensorDataArray;
	}

	addSensor(oSensorObject){
		this.sensorArray.push(oSensorObject);
		this.startPlan(null, this.sensorArray);
	}
	removeSensor(index){
		this.sensorArray.splice(index-1,1);
		this.startPlan(null, this.sensorArray);
	}
	addSwitch(oSwitchObject){
		this.switchesArray.push(oSwitchObject);
		this.startPlan(this.switchesArray, null);
	}
	removeSwitch(index){
		this.switchesArray.splice(index-1,1);
		this.startPlan(this.switchesArray, null);
	}
	consoleAll (){
		console.log(this.sensorArray)
		console.log(this.switchesArray)
	}
	getSensorsStatus (){
		return this.sensorArray;
	}
	getSwitchsStatus (){
		return this.switchesArray;
	}
	translateRequest (req){
	//	console.log(typeof req)
		if(typeof req == 'object')
			return req;
		
		return JSON.parse('{"' + decodeURI(req).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}')
	}
	startPlan(switchesArray, sensorArray){
		let that = this;

		if(switchesArray != null)
			this.switchesArray = switchesArray;
		
		if(sensorArray != null)
			this.sensorArray = sensorArray;


		clearInterval(this.interval);
		//this.interval = setInterval(function(){
			// Switches
			if(switchesArray != undefined && switchesArray.length >0){
				switchesArray.forEach(function(switchObject,index){
					if(switchObject.switch_type == 'instant' || switchObject.switch_type == undefined){
						that.turnSwitchOn(switchObject.switch_param);
					}
					if(switchObject.switch_type == 'timer'){
						setTimeout(function(){
							that.turnSwitchOn(switchObject.switch_param);
						}, switchObject.switch_start*1000)
						
					}
					if(switchObject.switch_type_end == 'instant' || switchObject.switch_type_end == undefined){
						that.turnSwitchOff(switchObject.switch_param);
						switchesArray.splice(index-1,1);
						that.switchesArray = switchesArray;
					}
					

					if(switchObject.switch_type_end == 'timer'){
						setTimeout(function(){
							that.turnSwitchOff(switchObject.switch_param);
							switchesArray.splice(index-1,1);
							that.switchesArray = switchesArray;
						}, (switchObject.switch_type == 'timer') ? switchObject.switch_start*1000+switchObject.switch_end*1000 : switchObject.switch_end*1000);
						
					}
				})
			}

			if(sensorArray != undefined && sensorArray.length > 0){
				sensorArray.forEach(function(sensorObject,index){
					that.interval[sensorObject.sensor_type+sensorObject.sensor_rate+sensorObject.sensor_param] = setInterval(function(){
						that.readSensorData(sensorObject.sensor_param, sensorObject.sensor_type, sensorObject.sensor_name);
					}, sensorObject.sensor_rate*1000);
				});
			}

	//	}, 1000);
	}
	readSensorData (pin, type, name){
		let that = this;
		//console.log(that.url+"arduino/"+type+"/"+pin)

		this.request(that.url+"arduino/"+type+"/"+pin, function (error, response, body) {
		  if (!error && response.statusCode == 200) {
		    let str = body;
		  	if(type=='analog'){
		  		let value = str.substring(
					    str.lastIndexOf("reads analog ") + 13, 
					    str.lastIndexOf("\r\n"));
		  		//console.log(value, 'sensor value');	
		  		that.saveSensorData(name, value);
		  	}
		    //console.log(body.body, 'DATA') 
		  }
		  //console.log(response, 'DATA')
		})

	}
	makeRequest (pin,state){
		let that = this;
		
		console.log(this.url+"arduino/digital/"+pin+"/"+state+'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX')

		this.request(this.url+"arduino/digital/"+pin+"/"+state, function (error, response, body) {
		  if (!error && response.statusCode == 200) {
		    //console.log(body, 'DATA') 
		  }
		  console.log(response, 'DATA')
		})
	}
	turnSwitchOn(pin){
		if(this.statuses[pin]==undefined || this.statuses[pin] != 0){
			this.makeRequest(pin, 0);
			this.statuses[pin] = 0;
		}
		
	}
	turnSwitchOff(pin){
		if(this.statuses[pin]==undefined || this.statuses[pin] != 1){
			this.statuses[pin] = 1;
			this.makeRequest(pin, 1);
		}
	}
}

module.exports = deviceManager;