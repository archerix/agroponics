const axios        = require('axios');
const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

class dataCollector {

	constructor(oDb, oRequest, io){
		//this.sensorDataArray = [];
        this.request = oRequest;
        this.socket = io;
		return this;
		
    }
    
    getPlantInfoFromTrefle(plantName){
        return axios
            .get('https://trefle.io/api/plants?q='+plantName+'&token=SXFUVS85VnpxaG5lRFNEenp5RVNYUT09')
            .catch(err => console.error(err));
    }


    getPlantGrowInfoFromTrefle(plantName){
        console.log('https://trefle.io/api/plants/'+plantName+'?token=SXFUVS85VnpxaG5lRFNEenp5RVNYUT09')
        return axios
            .get('https://trefle.io/api/plants/'+plantName+'?token=SXFUVS85VnpxaG5lRFNEenp5RVNYUT09')
            .catch(err => console.error(err));
    }


	getPlantInfo(plantName){
        let that = this;
        this.getPlantInfoFromTrefle(plantName).then(function(data){
            console.log(data.data)
            that.socket.emit('search_results', data.data);
        })
    }
    
    getPlantGrowData(plantId, save){
        let that = this;
        this.getPlantGrowInfoFromTrefle(plantId).then(function(data){
           
                MongoClient.connect('mongodb://localhost:27017', function(err, client) {
                    assert.equal(null, err);
                    console.log("Connected successfully to server");
                
                    const db = client.db('grow_data');
                    console.log('saving plant data in db...', data.data);
                    db.collection('plant').find({id : data.data.id}).toArray(function(err, result) {
                        if (err){
                         throw err;
                        }else{
                            if(result.length == 1){
                                console.log('result already exists legaly, ',result[0].images)
                                that.socket.emit('plant_info', result[0]);
                            }
                            console.log(result, 'RESULTS!!') 
                            if(result.length == 0){
                                if(save){
                                    db.collection('plant').insertOne(data.data, function(err, res) {
                                        if (err) throw err;
                                        console.log("1 document inserted");
                                    });
                                } else {
                                    that.socket.emit('plant_info', data.data);
                                }
                            }
                        }
                    });
                        
                    
                    
                });
        })
	}
}

module.exports = dataCollector;