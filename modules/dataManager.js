const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
const promise = require('promise');

class dataManager {

    constructor (){
        this.growPlanCollection = null;
        this.growPlanIdentifier = null;
    }

    setGrowPlanCollection (oObject){
        this.growPlanCollection = oObject;
    }

    getGrowingPlan (){
        let that = this;
        return new Promise(function(resolve, reject) {
        
        MongoClient.connect('mongodb://localhost:27017',{useNewUrlParser: true}, function(err, client) {
            assert.equal(null, err);        
            let db = client.db('grow_data');
            let planCollection = db.collection('grow_plan');
            that.setGrowPlanCollection(planCollection);
            planCollection.find({}, function(err, result) {
                    if (err) reject(err);
                    resolve(result.toArray());
                });
            })    
        });
    }

    saveIntoGrowingPlan (oldData, newData){
        let that = this;
        return new Promise(function(resolve, reject) {
            if(that.growPlanCollection !== null){
                
                let newvalues = { $set: newData };
                that.growPlanCollection.updateOne(oldData, newvalues, function(err, res) {
                    if (err) throw err;
                    console.log("1 document updated");
                    db.close();
                });

            }

        });
    }

}

module.exports = dataManager;