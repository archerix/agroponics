const express        = require('express');
const port = 8000;
const app            = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const schedule = require('node-schedule');
const bodyParser = require('body-parser');


let router = express.Router()

var request = require('request');
var ip = require('ip');
const deviceManager = require('./modules/deviceManager');
//const socketManager = require('./modules/socketManager');

const dataCollector = require('./modules/plants/dataCollector');
const plansManager = require('./modules/plansManager');

//const {env,mysqlInfo} = require('./conf/config');


function traceDeviceOnNetwork (ip){
	return new Promise(function(resolve, reject) {	
	let containingString = 'Pin D1 set to';
	let urlString = '/arduino/digital/1';
	console.log(ip.address());
	let ipArray = ip.address().split(".")

	for(var i=0;i<255;i++){
		let testingIpAddress = 'http://'+ipArray[0]+'.'+ipArray[1]+'.'+ipArray[2]+'.'+i;
		console.log(testingIpAddress);
		request(testingIpAddress+urlString, function (error, response, body) {
		  if (!error && response.statusCode == 200) {
		    console.log(body)
		    if(body.includes(containingString)){
				console.log('Device is on ip: '+testingIpAddress);
		    	this.url = testingIpAddress;
		    	resolve(testingIpAddress);
		    }
		  }
		  
		})
	}
	});
}


//let dropship = require('./modules/dropshipping');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

	

//app.use('/screenshots', express.static('screenshots'))

http.listen(port, () => {

	// middleware that is specific to this router
	app.use(function timeLog (req, res, next) {
		let ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
	 	//console.log( 'From: ', ip, 'time: ', dateFormat(Date.now(),'yyyy-mm-dd HH:MM:ss'),'Accessed: ', req.originalUrl);
	 	next()
	})


	console.log('We are live on ' + port);

	// app.get('/t', function(req, res){
	//   res.send('hello world :)'); 
	// });
	//app.use('/dropship', dropship

	app.get('/pinger', function(req, res){
		console.log('App was looking for me!')
	  res.send('DeviceManager is here'); 
	});
	
	app.post('/addSwitch', function(req, res){
	  console.log(req.body);
	  res.send(req.body); 
	});


	//traceDeviceOnNetwork(ip).then(function(result) {
		
		
		let oDeviceManager = new deviceManager('http://192.168.1.4/', request, ip);
		let oPlansManager = new plansManager(oDeviceManager);
		
		//let intervalContainer = null; // Take the lower one down
		io.on('connection', function(socket){
			let oDataCollector = new dataCollector(null, request, socket );	
			
			////////////////////////////////////////////////////////////////////////
			// HOWTO load plan by day id
			setTimeout(function(){
				 //console.log(oPlansManager.loadPlanByDayId(2));
				 oPlansManager.startPlan()
			 }, 4000)

			oDeviceManager.startPlan(null, null);

			let currentArrays = {'sensors': oDeviceManager.getSensorsStatus().slice(0), 'switches': oDeviceManager.getSwitchsStatus().slice(0)};
	        console.log('socket connection up');

			socket.on('search', function (searchString){
	        	console.log(searchString[0]);
				oDataCollector.getPlantInfo(searchString[0])
			})
			
			socket.on('getPlantById', function (plantId){
	        	console.log(plantId);
				oDataCollector.getPlantGrowData(plantId, false)
			})
			socket.on('savePlantById', function (plantId){
				let plantData = oDataCollector.getPlantGrowData(plantId, true)
				
			});	

	        socket.on('sensor_view', function (sensorName){
	        	console.log(sensorName);
	        	let data = oDeviceManager.getSensorData(sensorName)
	        	console.log(data);
	        	io.emit('sensor_data', data);
	        })

	        socket.on('sensor_add', function(sensor){
			  console.log(sensor)
			  oDeviceManager.addSensor(oDeviceManager.translateRequest(sensor))
			  console.log('sensor list is on its way!')
			  io.emit('sensors_list', oDeviceManager.getSensorsStatus());
			});

			socket.on('sensor_remove', function(sensor){
			  oDeviceManager.removeSensor(sensor.id)
			  console.log('sensor list is on its way!')
			  io.emit('sensors_list', oDeviceManager.getSensorsStatus());
			});

			let intervalContainer = null;
			let fullSensorData = null;



			if(intervalContainer == null){
			// Handle internal deviceManager
				intervalContainer = setInterval(function(){
					// check if data here is the same as in device manager
					if(JSON.stringify(currentArrays.sensors)!=JSON.stringify(oDeviceManager.getSensorsStatus())){
						console.log('sensor change!')
						io.emit('sensors_list', oDeviceManager.getSensorsStatus());
						currentArrays.sensors = oDeviceManager.getSensorsStatus().slice(0);
					}
					if(JSON.stringify(currentArrays.switches)!=JSON.stringify(oDeviceManager.getSwitchsStatus())){
						console.log('switch change!')
						io.emit('switch_list', oDeviceManager.getSwitchsStatus());
						currentArrays.switches = oDeviceManager.getSwitchsStatus().slice(0);
					}

					
				},1000);
			}

			socket.on('sensor_view', function(sensor){
			  io.emit('sensors_list', oDeviceManager.getSensorsStatus());
			  //io.emit('tweet', tweet);
			});

	        socket.on('switch_add', function(swtch){
			  console.log('switch adding')
			  oDeviceManager.addSwitch(oDeviceManager.translateRequest(swtch));
			  io.emit('switch_list', oDeviceManager.getSwitchsStatus());
			  //io.emit('tweet', tweet);
			});

			socket.on('switch_remove', function(switchObj){
			  oDeviceManager.removeSwitch(switchObj.id)
			  console.log('switch list is on its way!')
			  io.emit('switch_list', oDeviceManager.getSwitchsStatus());
			});

			socket.on('switch_view', function(switchObj){
				console.log('switch list is on its way!')
			  io.emit('switchs_list', oDeviceManager.getSwitchsStatus());
			  //io.emit('tweet', tweet);
			});

	        socket.on('console', function(swtch){
			  oDeviceManager.consoleAll()
			  //io.emit('tweet', tweet);
			});


	        //sock.emit('callEntity', oResult);
	    }); 
		
	        
	   /* }, function(err) {
	        console.log(err);
	    })
*/

	


});